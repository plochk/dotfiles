" Neovim config
"
" user file: /home/$USER/.config/nvim/init.vim
" global:    /etc/xdg/nvim/sysinit.vim
"

" Set Leader Key
let mapleader =" "
nnoremap <leader>n :NERDTree<CR>

" If editing .py: run Flake8 on save
autocmd BufWritePost *.py call Flake8() "runs PEP8 & syntax when saving

" Remove trailing whitespaces
autocmd BufWritePre * :%s/\s\+$//e

" Use deoplete autocomplete
let g:deoplete#enable_at_startup = 1

" View LaTeX document
map <leader>v :! mupdf $(echo % \| sed 's/tex$/pdf/') &<CR><CR>

" Basic settings
"set nospell spelllang=en_us
set cc=79 " Marker at 79 columns
syntax on
set title
set nowrap
set linebreak " do not line-break midword
set noerrorbells
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set autoindent
set smartindent
set number relativenumber
set noswapfile
set incsearch

" Plugins
call plug#begin('~/.config/nvim/plugged')

Plug 'lervag/vimtex'
Plug 'nvie/vim-flake8'
Plug 'preservim/nerdtree'
Plug 'jreybert/vimagit'

if has('nvim')
        Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
        Plug 'Shougo/deoplete.nvim'
        Plug 'roxma/nvim-yarp'
        Plug 'roxma/vim-hug-neovim-rpc'
endif

call plug#end()

" Fixes mouse issues on alacritty, seems not to work in nvim
"set ttymouse=sgr

"=============================================================================
" LaTeX
"
" Autocompletion
call deoplete#custom#var('omni', 'input_patterns', {
      \ 'tex': g:vimtex#re#deoplete
      \})
" PDF previewing
let g:vimtex_view_general_viewer = 'mupdf'
let g:vimtex_view_general_options = '-reuse-instance @pdf'
let g:vimtex_view_general_options_latexmk = '-reuse-instance'
