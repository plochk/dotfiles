-- TEST
import XMonad
import Data.Monoid
import System.Exit
import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import System.IO (hPutStrLn)

import XMonad.Actions.WithAll -- killAll, sinkAll
-- Utils
import XMonad.Util.Run(spawnPipe, safeSpawn)
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig(additionalKeysP)

-- Hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog

import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders (smartBorders, noBorders)


-- Master Key.
-- mod1Mask ("left alt"); mod3Mask ("right alt"  ); mod4Mask ("windows key")
myModMask :: KeyMask
myModMask = mod4Mask

-- Run prompt
myRunPrompt :: String
myRunPrompt = "dmenu_run -fn 'Hack-14' -nb black -sf white -sb '#33af5a'"

-- Set default terminal
myTerminal :: String
myTerminal = "alacritty"
--myTerminal = "konsole"

myBrowser :: String
myBrowser = "firefox"

myEditor :: String
myEditor = "nvim"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Border width myBorderWidth :: Dimension
myBorderWidth = 4

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces :: [String]
myWorkspaces    = ["1:tty","2:web","3:file","4","5","6","7","8","9:media"]

-- Border colors for unfocused and focused windows
--
myNormalBorderColor  = "black"
myFocusedBorderColor = "white"

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
      [
    -- launch a terminal
--      ((modm,               xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu, run launcher
--    , ((modm,               xK_d     ), spawn "dmenu_run -fn 'Hack-14' -nb black -sf white -sb '#33af5a'")

    -- launch gmrun
    --, ((modm .|. shiftMask, xK_d   ), spawn "gmrun")

    -- launch nmcli_dmenu
    --, ((modm,               xK_p   ), spawn "nmcli_dmenu")

    -- lock the screen
--    , ((modm,               xK_x     ), spawn "xscreensaver-command -lock")

    -- take screenshot
--    , ((modm,               xK_Print ), spawn "spectacle -r -b -c")
    --, ((modm, xK_Print               ), spawn "scrot '%Y-%m-%d_$wx$h.png' -e 'mv $f ~/Pictures/'")

    -- emacs
    --, ((modm,               xK_ ), spawn "emacs")

    -- thunderbird mail
    --, ((modm,               xK_ ), spawn "thunderbird")

    -- firefox
    --, ((modm,               xK_ ), spawn "firefox")
    
    -- close focused window
--    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
--    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
      ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
--    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
--    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
--    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
--    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
--    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
--    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
--    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
--    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
--    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Xmonad
--    , ((modm .|. shiftMask, xK_q), io (exitWith ExitSuccess)) -- Quit Xmonad
--    , ((modm              , xK_q), spawn "xmonad --recompile; xmonad --restart") -- Restart Xmonad

    -- Multimedia Keys
    -- Vendor Bindings in: /usr/include/X11/XF86keysym.h
--    , ((0, 0x1008FF11), spawn "amixer set Master 5%- unmute") --XF86XK_AudioLowerVolume
--    , ((0, 0x1008FF13), spawn "amixer set Master 5%+ unmute") --XF86XK_AudioRaiseVolume
--    , ((0, 0x1008FF12), spawn "amixer set Master mute" )      --XF86XK_AudioMute
--    , ((0, 0x1008FF03), spawn "lux -s 50")                    --XF86XK_MonBrightnessDown
--    , ((0, 0x1008FF02), spawn "lux -a 50")                    --XF86XK_MonBrightnessUp

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]   
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


myKeys2 :: [(String, X ())]
myKeys2 =
      -- Xmonad
          [ ("M-C-r", spawn "xmonad --recompile") -- Recompile xmonad
          , ("M-q", spawn "xmonad --restart ")    -- Restart xmonad
          , ("M-S-q", io (exitWith ExitSuccess))  -- Quit xmonad

      -- Run prompt
          , ("M-d", spawn myRunPrompt) -- dmenu

      -- Program Shortcuts
          , ("M-<Return>", spawn myTerminal) -- Terminal
          , ("M-x", spawn "xscreensaver-command -lock") -- Screensaver
          , ("M-<Print>", spawn "spectacle -r -b -c") -- Screenshot


      -- Killing windows
          , ("M-S-c", kill)    -- kill focused window
          , ("M-S-a", killAll) -- kill all windows on workspace

      -- Floating windows
          , ("M-t", withFocused $ windows . W.sink ) -- focused window to tiling
          , ("M-S-t", sinkAll) -- all windows on workspace to tiling

      -- Increase/decrease spacing (gaps)
          , ("M-o", decWindowSpacing 4)     -- Decrease window spacing
          , ("M-p", incWindowSpacing 4)     -- Increase window spacing
          , ("M-S-o", decScreenSpacing 4)   -- Decrease screen spacing
          , ("M-S-p", incScreenSpacing 4)   -- Increase screen spacing

      -- Window Navigation
         , ("M-j", windows W.focusDown)    -- Move focus to the next window
         , ("M-k", windows W.focusUp)      -- Move focus to the prev window
         , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
         , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window

      -- Window resizing & Number of Masters
          , ("M-h", sendMessage Shrink)   -- Shrink horiz window width
          , ("M-l", sendMessage Expand)   -- Expand horiz window width
          , ("M-,", sendMessage (IncMasterN 1)) -- Increment amount of windows in master pane
          , ("M-.", sendMessage (IncMasterN (-1))) -- Decrement amount of windows in master pane
         -- , ()

      -- Layouts
         , ("M-<Space>", sendMessage NextLayout) -- Rotate through layout algorithms

      -- Applications (CTRL-e followed by a key)
         , ("M-e e", spawn "emacsclient -c -a 'emacs'")                            -- start emacs
         , ("M-e d", spawn "emacsclient -c -a 'emacs' --eval '(dired nil)'")       -- dired emacs file manager
         , ("M-e f", spawn "firefox")                      
         , ("M-e t", spawn "thunderbird")                      
         , ("M-e b", spawn "sh -c 'flatpak run com.bitwarden.desktop'")                      

      -- Multimedia Keys
         , ("<XF86AudioMute>",   spawn "amixer set Master toggle")
         , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
         , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
         , ("<XF86Search>", safeSpawn "firefox" ["https://www.duckduckgo.com/"])
         , ("<XF86AudioMicMute>", spawn "amixer set Capture toggle")
         , ("<XF86MonBrightnessUp>", spawn "lux -a 50")
         , ("<XF86MonBrightnessDown>", spawn "lux -s 50")

         ]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = avoidStruts (smartBorders (tiled ||| Full ||| Mirror tiled)) ||| noBorders Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = spacing myGapSize $ Tall nmaster delta ratio
     -- The default number of windows in the master pane
     nmaster = 1
     -- Default proportion of screen occupied by master pane
     ratio   = 1/2
     -- Percent of screen to increment by when resizing panes
     delta   = 3/100
     -- Border Gap Size
     myGapSize = 10

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook xmproc = dynamicLogWithPP xmobarPP {
    ppOutput  = hPutStrLn xmproc
  , ppTitle   = xmobarColor "grey" "" . shorten 50     -- Title of active window
--  , ppCurrent = xmobarColor "#03af1a" "" . wrap "[""]" -- Current workspace
--  , ppCurrent = xmobarColor "#fb4934" "" . wrap "[""]" -- Current workspace
  , ppCurrent = xmobarColor "#88ffb6" "" . wrap "[""]" -- Current workspace
--  , ppVisible = xmobarColor "grey" ""                  -- Visible but not current workspace
--  , ppHidden  = xmobarColor "grey" "" . wrap "*" ""    -- Hidden workspaces
--  , ppHiddenNoWindows = xmobarColor "grey" ""          -- Hidden workspaces (no windows)
  , ppUrgent  = xmobarColor "ff0066" "" . wrap "!" ""  -- Urgent workspace
  }

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
     spawnOnce "/usr/bin/nitrogen --restore &" -- wallpaper
     spawnOnce "/usr/bin/compton &"          -- enable transparency
     spawnOnce "/usr/bin/emacs --daemon"     -- emacs server
     spawnOnce "/usr/bin/trayer --edge top --align right --SetDockType true --SetPartialStrut true --expand true --width 4,4 --transparent true  --alpha 0 --tint 0x000000 --height 30 &"
     spawnOnce "/usr/bin/lxsession &"
     spawnOnce "~/Applications/syncthing/syncthing serve --no-browser --logfile=default &"
     spawnOnce "/usr/bin/qsyncthingtray &"
     spawnOnce "/usr/bin/xscreensaver -no-splash &"
     spawnOnce "~/Applications/xflux -l 48.254143 -g 11.555079 -r 1"
     spawnOnce "ssh-agent &"

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do
  xmproc <- spawnPipe "/usr/bin/xmobar ~/.config/xmobar/xmobarrc"
  xmonad $ docks defaults
      {
        logHook = myLogHook xmproc
      } `additionalKeysP` myKeys2

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        --logHook            = myLogHook,
        startupHook        = myStartupHook
    }

-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
