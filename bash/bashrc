#***********************************#
#           .bashrc                 #
#                                   #
# When read?:                       #
#     in every shell/subshell       #
#                                   #
# Where to deploy?:                 #
#     ~/.bashrc                     #
#                                   #
#***********************************#


# Not running interactively, do nothing
case $- in
    *i*) ;;
      *) return;;
esac


#=============================================================================
# Exports

# Custom Prompt
#PS1="[\u@\h \W]\\$ \[$(tput sgr0)\]"
PS1="\[\033[38;5;202m\][\W]\[$(tput sgr0)\] \[$(tput sgr0)\]"

# MAN
#export MANPAGER='sh -c "bat --theme GitHub -l man -p"'

# already set in /etc/profile
#export MINICOM='-con'

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=0'

#=============================================================================
# Path

PATH=$PATH:/home/plochk/.local/bin

#=============================================================================
# Environment Sourcing

# completion features
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

# Support for external aliases file
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Rust
if [ -f "${HOME}/.cargo" ]; then
    source "${HOME}/.cargo/env"
fi

# Alacritty shell flag completion
if [ -f "${HOME}/src/alacritty/extra/completions/alacritty.bash" ]; then
    source "${HOME}/src/alacritty/extra/completions/alacritty.bash"
fi

#=============================================================================
# Functions

# Countdown, input in seconds
# Usage: countdown 60 -> 1 Minute
#        countdown $((60*25)) -> 25 Minutes
#        countdown $(60*60) -> 1 Hour
function countdown()
{
   date1=$((`date +%s` + $1));
   while [ "$date1" -ge `date +%s` ]; do
     echo -ne "$(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r";
     sleep 0.1
   done
}

function stopwatch()
{
  date1=`date +%s`;
   while true; do
    echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r";
    sleep 0.1
   done
}

# Extraction function
# Usage: ex <file>
function ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf "$1"    ;;
      *.tar.gz)    tar xzf "$1"    ;;
      *.bz2)       bunzip2 "$1"    ;;
      *.rar)       unrar x "$1"    ;;
      *.gz)        gunzip "$1"     ;;
      *.tar)       tar xf "$1"     ;;
      *.tbz2)      tar xjf "$1"    ;;
      *.tgz)       tar xzf "$1"    ;;
      *.zip)       unzip "$1"      ;;
      *.Z)         uncompress "$1" ;;
      *.7z)        7z x "$1"       ;;
      *.deb)       ar x "$1"       ;;
      *.tar.xz)    tar xf "$1"     ;;
      *.tar.zst)   unzstd "$1"     ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Load up ssh-agent and read the specified keyfile in ~/.ssh/
function ssh-load ()
{
    # TODO: check whether the agent is already running
    eval $(ssh-agent)
    ssh-add "${HOME}/.ssh/${1}"
}

#=============================================================================
# Aliases

if command -v doas &> /dev/null ; then
    alias sudo=doas
fi

# refresh font cache
alias fontupdate='sudo fc-cache -fv'

# Movement
alias ".."="cd .."
alias "..."="cd ../.."
alias ".4"="cd ../../.."
alias ".5"="cd ../../../.."

# Colorize grep output

# Basic commands overhaul
alias ls='ls -Fh --color=auto'
alias ll='ls -laFh --color=auto'

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
